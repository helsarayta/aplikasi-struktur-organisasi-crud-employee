package com.heydie.crudemployee;

import com.heydie.crudemployee.entity.Employee;
import com.heydie.crudemployee.repository.EmployeeRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class EmployeeRepositoryTest {
    @Autowired private EmployeeRepository repo;

    @Test
    public void testAddNew() {
        Employee employee = new Employee();
        employee.setId(10);
        employee.setNama("Helsa");
        employee.setAtasanId(3);
        employee.setCompanyId(1);

        Employee resultSave = repo.save(employee);

        Assertions.assertThat(resultSave).isNotNull();
        Assertions.assertThat(resultSave.getId()).isGreaterThan(0);

    }

    @Test
    public void testFindAll() {
        Iterable<Employee> employees = repo.findAll();
        Assertions.assertThat(employees).hasSizeGreaterThan(0);

        for (Employee emp : employees) {
            System.out.println(emp);
        }
    }

    @Test
    public void testUpdateData() {
        Integer employeeId = 10;
        Optional<Employee> employee = repo.findById(employeeId);
        Employee employee1 = employee.get();
        employee1.setNama("Alvaro");
        repo.save(employee1);

        Employee updated = repo.findById(employeeId).get();
        Assertions.assertThat(updated.getNama()).isEqualTo("Alvaro");
    }

    @Test
    public void testGet() {
        Integer employeeId = 10;
        Optional<Employee> employee = repo.findById(employeeId);
        Assertions.assertThat(employee).isPresent();
        System.out.println(employee.get());
    }

    @Test
    public void testDelete() {
        Integer employeeId = 10;
        repo.deleteById(employeeId);

        Optional<Employee> employee = repo.findById(employeeId);
        Assertions.assertThat(employee).isNotPresent();

    }
}
