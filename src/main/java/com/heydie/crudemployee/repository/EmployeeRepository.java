package com.heydie.crudemployee.repository;

import com.heydie.crudemployee.entity.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    public Long countById(Integer id);
}
