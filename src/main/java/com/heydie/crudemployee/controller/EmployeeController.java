package com.heydie.crudemployee.controller;

import com.heydie.crudemployee.entity.Employee;
import com.heydie.crudemployee.service.EmployeeNotFoundException;
import com.heydie.crudemployee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employee")
    public String showEmployeeList(Model model) {
        List<Employee> employeeList = employeeService.listAll();
        model.addAttribute("employeeList", employeeList);

        return "employee";
    }

    @GetMapping("/employee/new")
    public String showNewForm(Model model) {
        model.addAttribute("employee", new Employee());
        model.addAttribute("pageTitle", "Add New Employee");
        return "employee_form";
    }

    @PostMapping("employee/save")
    public String saveEmployee(Employee employee, RedirectAttributes attributes) {
        employeeService.save(employee);
        attributes.addFlashAttribute("message", "New Employee has been saved succesfully");

        return "redirect:/employee";
    }

    @GetMapping("/employee/edit/{id}")
    public String showEditForm(@PathVariable("id") Integer id, Model model, RedirectAttributes attributes) {
        try {
            Employee employee = employeeService.get(id);
            model.addAttribute("employee", employee);
            model.addAttribute("pageTitle", "Edit Employee ID : " + id);
            return "employee_form";
        } catch (EmployeeNotFoundException e) {
            attributes.addFlashAttribute("message", e.getMessage());
            return "redirect:/employee";
        }
    }

    @GetMapping("/employee/delete/{id}")
    public String deleteEmployee(@PathVariable("id") Integer id, RedirectAttributes attributes) {
        try {
            employeeService.delete(id);
            attributes.addFlashAttribute("message", "User ID "+id+" has been deleted");
        } catch (EmployeeNotFoundException e) {
            attributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/employee";
    }
}


