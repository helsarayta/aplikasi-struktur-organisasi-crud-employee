package com.heydie.crudemployee.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 50)
    private String nama;

    @Column(length = 5)
    private Integer atasanId;

    @Column(length = 5)
    private Integer companyId;

}
