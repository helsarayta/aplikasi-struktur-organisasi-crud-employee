package com.heydie.crudemployee.service;

import com.heydie.crudemployee.entity.Employee;
import com.heydie.crudemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> listAll() {
        return (List<Employee>) employeeRepository.findAll();
    }

    public void save(Employee employee) {
        employeeRepository.save(employee);
    }

    public Employee get(Integer id) throws EmployeeNotFoundException {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if(employeeOptional.isPresent()) {
            return employeeOptional.get();
        }
        throw new EmployeeNotFoundException("Could not find any Employee with ID " + id);
    }

    public void delete(Integer id) throws EmployeeNotFoundException {
        Long count = employeeRepository.countById(id);
        if (count == null || count == 0) {
            throw new EmployeeNotFoundException("Could not find any Employee with ID " + id);
        }
        employeeRepository.deleteById(id);

    }
}
